var DayAdvent = {
	absTotal: function(tot, val)
	{
		return Math.abs(val) + tot;
	},
	performPart1: function(output, currentValue) {
		currentValue = this.trim(currentValue);
		
		var walker = currentValue.split(", ");
		/* currentDirection 0 = N, 1 = E, 2 = S, 3= W */
		
		var res = walker.reduce(this.performWalkPart1.bind(this), {currentDirection: 0, currentPosition: [0,0], totDist:0})
		output.push(JSON.stringify(res));
		
		return output;
		
	},
	performWalkPart1: function(accumulator, currentValue) {
		var direction = currentValue[0];
		var length = currentValue.substring(1) * 1;
		accumulator.currentDirection = this.determineDirection(direction, accumulator.currentDirection);
		
		if ( accumulator.currentDirection > 1 )
		{
			length = length * -1;
		}
		accumulator.currentPosition[accumulator.currentDirection % 2] += length;
		accumulator.totDist = accumulator.currentPosition.reduce(this.absTotal, 0);				
		return accumulator;				
	},
	performPart2: function(output, currentValue) {	
		currentValue = this.trim(currentValue);
		
		var walker = currentValue.split(", ");
		var stops = {"0_0": true};
		var currentInfo = {currentDirection: 0, currentPosition: [0,0], totDist:0, finalStop:""}
		walker.some(this.performWalkPart2.bind(this, currentInfo, stops));
		output.push(JSON.stringify(currentInfo));
		
		return output;

	},
	determineDirection: function(inputDirection, currentDirection)
	{
		
		/* currentDirection 0 = N, 1 = E, 2 = S, 3= W */

		if ( inputDirection == "L")
		{
			currentDirection--; 
		} else if ( inputDirection == "R" )
		{					
			currentDirection++;
		}
		
		currentDirection = currentDirection+4;
		currentDirection = Math.abs(currentDirection) % 4;
		return currentDirection;
	},
	performWalkPart2: function(currentInfo, stops, currentValue)
	{
		var direction = currentValue[0];
		var length = currentValue.substring(1) * 1;
		currentInfo.currentDirection = this.determineDirection(direction, currentInfo.currentDirection);
		
		var tmpPos = [currentInfo.currentPosition[0], currentInfo.currentPosition[1]];
		var step = 0;
		for ( var i = 0; i < length; i++)
		{
			
			if ( currentInfo.currentDirection > 1 )
			{
				step = -1;
			} else {
				step = +1;
			}
			tmpPos[currentInfo.currentDirection % 2] += step;
			
			var stopString = tmpPos[0].toString() + "_" + tmpPos[1].toString();
			if ( ! stops[stopString])
			{
				stops[stopString] = true;
			} else {
				currentInfo.finalStop = stopString; 
				currentInfo.finalDist = tmpPos.reduce(this.absTotal, 0);
					Math.abs(tmpPos[0]) + Math.abs(tmpPos[1]);
				return true;
			}	
		}
		
		if ( currentInfo.currentDirection > 1 )
		{
			length = length * -1;
		}
		
		
		currentInfo.currentPosition[currentInfo.currentDirection % 2] += length;
		currentInfo.totDist = currentInfo.currentPosition.reduce(this.absTotal, 0);
		
		return false;				

	}
}


try {

 	module.exports = DayAdvent;

} catch (e) {

	Advent = Advent.extend(Advent,  DayAdvent);
}

