var DayAdvent = {
	aCharCode: "a".charCodeAt(),
	createChecksum: function(room)
	{
		var roomEncoded = room.split(/\d/)[0];
		roomEncoded = roomEncoded.replace("-", "");
		var chars = []
		for ( var i = 0; i < 26; i++)
		{
			var tmpChar = String.fromCharCode(this.aCharCode+i);
			var re = new RegExp(tmpChar, "g");
			var newRoomEncoded = roomEncoded.replace(re, "");
			var numMatches = roomEncoded.length - newRoomEncoded.length;
			if (numMatches > 0)
			{
				chars.push({"char": tmpChar, "len": numMatches});
				roomEncoded = newRoomEncoded;
			}
			if ( roomEncoded.length === 0)
				break;
		}
		chars.sort(this.compareCheckSum.bind(this));
		return  chars.slice(0,5).map(this.strOnly.bind(this)).join("");

	},
	isValidRoom: function(room)
	{
		var actualCheckSum = this.createChecksum(room);
		var listedCheckSum = room.split(/\[|\]/)[1];
		if ( actualCheckSum === listedCheckSum)
		{
			return true;
		} else {
			return false;
		}
	},

	decryptRoom: function(room)
	{
			var roomEncoded = room.split(/\d/)[0];
			var roomValue = this.roomValue(room);
			return roomEncoded.split("").map(this.bitShift.bind(this, roomValue)).join("");		
	
	},
	bitShift: function(roomValue, char)
	{
		if ( char === "-" )
			return " ";
		var	aBasedValue = ((char.charCodeAt() - this.aCharCode) + roomValue ) % 26;
		return String.fromCharCode(this.aCharCode + aBasedValue)
	},
	roomValue: function(room)
	{
		var val = room.replace(/\D/gi, "");
		return Number(val);
	},

	strOnly: function(currentValue)
	{
		return currentValue.char;
	},
	compareCheckSum: function(a, b)
	{
		if ( a.len === b.len)
		{
			return   a.char.charCodeAt() - b.char.charCodeAt();
		} else {
			return   b.len - a.len; 
		}
	},
	performPart1: function(output, currentValue)
	{
		currentValue = this.trim(currentValue);
		if ( this.isValidRoom(currentValue))
		{
			output.push(this.roomValue(currentValue));
		}
			
			
			
			
		return output;
	},
	performPart2: function(output, currentValue)
	{
		currentValue = this.trim(currentValue);

		if ( this.isValidRoom(currentValue)) {
			
		var res = {"itm":currentValue, decryptRoom: this.decryptRoom(currentValue)};
		
		if ( res.decryptRoom.indexOf("north") >= 0 )
			output.push(JSON.stringify(res));
			
			
		}
			
			
		return output;

	}
	,
	sum: function(accumulator, currentValue)
	{
		if ( accumulator === undefined)
			accumulator = 0;
		return accumulator + currentValue;
	},
	Part1: function()
	{
		
			
		var vals = this.GetInput().split("\n");
		var outStr = [];
		
		 outStr = vals.reduce(this.performPart1.bind(this), []);
		
		var sum = outStr.reduce(this.sum.bind(this))
		outStr.length = 0;
		outStr.push(sum);
		outStr.push("end");
		this.WriteOutput(outStr);
	
	},
	Part2: function()
	{
		var vals = this.GetInput().split("\n");
		var outStr = [];
		
		 outStr = vals.reduce(this.performPart2.bind(this), []);
		
		
		outStr.push("end");
		this.WriteOutput(outStr);
	}
	
}


try {

 	module.exports = DayAdvent;

} catch (e) {

	Advent = Advent.extend(Advent,  DayAdvent);
}

