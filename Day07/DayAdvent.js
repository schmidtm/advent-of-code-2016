var DayAdvent = {
	getHyperNets: function(inStr)
	{
		var hypers = [];
		var findHypernet = /\[([a-z]*)\]/gi;
		var match = null;
		while ( match = findHypernet.exec(inStr) )
		{
			hypers.push(match[1])
		}
		return hypers;
	},
	getExtraNets: function(inStr)
	{
		return inStr.split(/\[[a-z]*\]/gi);
	},
	isABBAinHyperNet: function(inStr)
	{
		var hypers = this.getHyperNets(inStr).join("|");
		return  this.isABBA(hypers);
	},
	isABBA: function(inStr)
	{
		var regExp = /([a-z])(?!\1)([a-z])\2\1/gi /* Finds ABBA  discludes letter 0 and letter 1 from matching*/
		var match = regExp.exec(inStr);
		return match !== null;
	},
	matchesCount:0,
	supportTLS: function(inStr)
	{
		if ( ! this.isABBAinHyperNet(inStr))
		{
			if ( this.isABBA(inStr))
			{
				return true;
			}
		}
		return false;
	},
	supportSSL: function(inStr)
	{
		var hypers = this.getHyperNets(inStr).join("|");
		var extraNets = this.getExtraNets(inStr).join("|");
		return this.getABAs(extraNets, hypers);
	},
	getABAs: function(extraNets, hypers)
	{
		
		var regExp = /(([a-z])(?!\2)[a-z]\2)/gi; /* FINDs AbA   discludes letter 0 and letter 1 from matching*/
		var potHyperString = "";
		while ( match = regExp.exec(extraNets))
		{
			potHyperString = match[1][1] + match[1][0] + match[1][1];
			if ( hypers.indexOf(potHyperString) >= 0)
				return true;

			regExp.lastIndex += -2; /* decrement index Counter by 2 to include the middle and second characterin next search */


		
		}
		return false;
	},
	performPart1: function(output, currentValue)
	{
		
		currentValue = this.trim(currentValue);
		if  (this.supportTLS(currentValue))
		{	
			this.matchesCount++;
			output.push(currentValue + " supports TLS");
		}	
			
		return output;
	},
	performPart2: function(output, currentValue)
	{
		currentValue = this.trim(currentValue);
		if  (this.supportSSL(currentValue))
		{	
			this.matchesCount++;
			output.push(currentValue + " supports SSL");
		}	
			
		return output;

	},
	Part1: function()
	{
		this.matchesCount = 0;
			
		var vals = this.GetInput().split("\n");
		var outStr = [];
		
		 outStr = vals.reduce(this.performPart1.bind(this), []);
		outStr = [];
		outStr.push(this.matchesCount);
		outStr.push("end");
		this.WriteOutput(outStr);
	
	},
	Part2: function()
	{
			this.matchesCount = 0;
			
		var vals = this.GetInput().split("\n");
		var outStr = [];
		
		 outStr = vals.reduce(this.performPart2.bind(this), []);
		outStr = [];
		outStr.push(this.matchesCount);
		outStr.push("end");
		this.WriteOutput(outStr);
	}
	
}


try {

 	module.exports = DayAdvent;

} catch (e) {

	Advent = Advent.extend(Advent,  DayAdvent);
}

