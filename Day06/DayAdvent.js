var DayAdvent = {
	part1Col : [],
	/* [{let:a, cnt:0}]*/
	
	aCharCode: "a".charCodeAt(),
	initLetterArr: function()
	{
		var ret = [];
		for ( var i = 0; i < 26; i++)
		{
			ret.push({let:String.fromCharCode(i+this.aCharCode), cnt:0});
		}
		return ret;
	},
	charVal: function(charIn)
	{
			return charIn.charCodeAt() - this.aCharCode;
	},
	performPart1: function(output, currentValue)
	{
		var arr = this.trim(currentValue).split("");	
		for ( var i = 0; i < arr.length; i++)
		{
			this.part1Col[i][this.charVal(arr[i])].cnt++;
		}		
		return output;
	},
	performPart2: function(output, currentValue)
	{
		var res = {"itm":currentValue};
		output.push(JSON.stringify(res));
			
			
			
			
		return output;

	},
	part1Pick: function(val)
	{
		val.sort(this.compareLet.bind(this));
		return val[0].let;
	},
	part2Pick:function(val)
	{
		val.sort(this.compareLet.bind(this))
		for ( var i = val.length - 1; i > 0; i--)
		{
			if ( val[i].cnt > 0)
			{
				return val[i].let;
			}
		}
		return val[val.length-1].let;
	},
	compareLet: function(a, b)
	{
		if ( a.cnt === b.cnt)
		{
			return   a.let.charCodeAt() - b.let.charCodeAt();
		} else {
			return   b.cnt - a.cnt; 
		}
	
	},
	Part1: function()
	{
		
		this.part1Col = [];
		var vals = this.GetInput().split("\n");
		var outStr = [];
		if ( vals.length )
		{
			for ( var i = 0; i < vals[0].length; i++)
			{
				this.part1Col.push(this.initLetterArr())
			}
		}
		 outStr = vals.reduce(this.performPart1.bind(this), []);
		var outArr = this.part1Col.map(this.part1Pick.bind(this))
		outStr.push(outArr.join(""));
		outStr.push("end");
		this.WriteOutput(outStr);
	
	},
	Part2: function()
	{
		
		this.part1Col = [];
		var vals = this.GetInput().split("\n");
		var outStr = [];
		if ( vals.length )
		{
			for ( var i = 0; i < vals[0].length; i++)
			{
				this.part1Col.push(this.initLetterArr())
			}
		}
		 outStr = vals.reduce(this.performPart1.bind(this), []);
		var outArr = this.part1Col.map(this.part2Pick.bind(this))
		outStr.push(outArr.join(""));
		outStr.push("end");
		this.WriteOutput(outStr);
	
	}
	
}


try {

 	module.exports = DayAdvent;

} catch (e) {

	Advent = Advent.extend(Advent,  DayAdvent);
}

