var DayAdvent = {
	performPart1: function(output, currentValue)
	{
		var vals = currentValue.split(" ");
		switch (vals[0])
		{
			case "board" :
				this.setupBoard(vals[1]);
				break;
			case "rect" :
				this.turnOnRect(vals[1]);
				break;
			case "rotate" :
				if ( vals[2].indexOf("y=") >= 0)
					this.rotateRow(vals[2], vals[4])
				else
					this.rotateCol(vals[2], vals[4])
				
				break;
		}
	
			
			this.outputBoard()
			
			
		return output;
	},

	board: undefined,
	convertStrToNum: function(currentValue) {
		return currentValue *1;
	},
	turnOnRect: function(dim)
	{
		var dims = dim.split("x").map(this.convertStrToNum.bind(this));
		var cnt = 0;
		for ( var i = 0; i < dims[0]; i++)
		{
			for ( var j = 0; j < dims[1]; j++)
			{
				cnt++;
				this.board[j][i] = "1";
			}
		}
		this.outputBoard();
	},
	getBoardDims: function()
	{
		return { y: this.board.length, x: this.board[0].length };
	},
	rotateRow: function(pos, dist) {
		var dims = this.getBoardDims();
		
		pos = this.convertStrToNum(pos.replace("y=", ""))
		dist = this.convertStrToNum(dist) % dims.x;
		for ( var i = 0; i < dims.x; i++)
		{
			var nxtPos =(dims.x+i-dist)%dims.x;
			this.board[pos][i] =  this.board[pos][nxtPos].split("_").pop() + "_"  + this.board[pos][i] ; 
		}
	
	this.cleanAfterRotate("y", pos);
	},
	rotateCol: function(pos, dist) {
		var dims = this.getBoardDims();

		pos = this.convertStrToNum(pos.replace("x=", ""))
		dist = this.convertStrToNum(dist) % dims.y;

	for ( var i = 0; i < dims.y; i++)
		{
			var nxtPos =(dims.y+i-dist)%dims.y;
			this.board[i][pos] =  this.board[nxtPos][pos].split("_").pop() + "_" +  this.board[i][pos] ; 
		}
	
		this.cleanAfterRotate("x", pos);

	},
	cleanAfterRotate: function(xOrY, pos)
	{
		var dims = this.getBoardDims();

		if ( xOrY === "y")
		{
			for ( var i = 0; i < dims.x; i++)
			{
				this.board[pos][i] = this.board[pos][i].split("_").shift(); 
			}
		} else {
			for ( var i = 0; i < dims.y; i++)
			{
				this.board[i][pos] = this.board[i][pos].split("_").shift(); 
			}
		}
	},
	setupBoard: function(dim)
	{
		/* dim = h x w */
		var x, y;
		// [x,y] = dim.split("x").map(this.convertStrToNum.bind(this)); /* Not Supported in IE */
		var dims = dim.split("x").map(this.convertStrToNum.bind(this));
		var x = dims[0];
		var y = dims[1];
		this.board = [];
		for ( var i = 0 ;i < y; i++)
		{
			this.board.push([]);
			for ( var j = 0; j < x; j++)
			{
				this.board[i].push("0");
			}
		}

		// this.board = new Array(dims[1]).fill(new Array(dims[0]).fill(0)); /* this used the same array for every row */	

		
	},

	outputBoard: function()
	{
		outStr = [];
		outStr = this.board.map(function(val) { return val.join("").replace(/0/gi, " " ).replace(/1/gi, "#")});

		outStr.push("Active Lights: " + outStr.join("").replace(/ /gi, "").length);
		this.WriteOutput(outStr);
	},
	output2DArray: function(arr)
	{
		outStr = [];
		outStr = arr.map(function(val) { return val.join("").replace(/0/gi, " " ).replace(/1/gi, "#")}).join("\n");
	
		return outStr;
	},
	Part1: function()
	{
		
			
		var vals = this.GetInput().split("\n");
		var outStr = [];
		
			this.setupBoard("50x6");

		 outStr = vals.reduce(this.performPart1.bind(this), []);
		
		this.outputBoard();
		
		outStr.push("end");
		//this.WriteOutput(outStr);
	
	},
	Part2: function()
	{
		this.Part1();
		/*
		 I could have the system figure out what the letters are but 
		 they are displayed in the output above I would have to know the entire alphabet in this font
		 */	
		var subArrayWidth = 5;
		var dims = this.getBoardDims(); 
		var subArrayHeight = dims.y;
		var numChars = dims.x / subArrayWidth;
		var chars = 0;
		var outStr = [];

		for ( var i = 0; i < numChars; i++)
		{
			var tmpChar = [];
			for ( var j=0;j<subArrayHeight;j++)
			{
				tmpChar.push(this.board[j].slice(i*subArrayWidth, (i+1)*subArrayWidth));
			}
			outStr.push(this.output2DArray(tmpChar));
			outStr.push("\n\n");
		}
		this.WriteOutput(outStr);

	}
}

try {

 	module.exports = DayAdvent;

} catch (e) {

	Advent = Advent.extend(Advent,  DayAdvent);
}

