"use strict"
const fs = require('fs');
var Advent = require('./Advent');
Advent.FileLoader = {
	LoadFile: function(filename, cbFn)
	{
	return new Promise(function(fullfill, reject) {			
		fs.readFile(filename, 'utf8', function(err, data) {
			if ( err ) reject(err);
			fullfill(data);
		})
		})
	}
}


var AdventNode = {
	LoadReadMe :  function()
	{
		return this.FileLoader.LoadFile("Day" + this.params.Day + "/README.txt" );
	},
	
	LoadDefaultInput : function()
	{
		return this.FileLoader.LoadFile("Day" + this.params.Day + "/Defaultinput.txt",  function(resp) {Advent.input = resp.responseText });
	},
	
	LoadInput :  function()
	{
		 return this.FileLoader.LoadFile("Day" + this.params.Day + "/input.txt",  function(resp) {Advent.input = resp.responseText  });
		
	},
	input : "",
	GetInput : function()
	{
		return this.input;
	},
	WriteOutput :  function(output)
	{
		var res = {value:""};
		if ( output instanceof Array)
			res.value = output.join("\n");
		else
			res.value =output;
		console.log(res.value);
	},
	params :  {"Day" : "01", "Part" : "1", DataSet:"Final" }
}

Advent = Advent.extend(Advent, AdventNode);
/* Arguments:
		Day=01 (Day)
		Part=1 | Part2 
		Example | Final 

*/
process.argv.forEach(function(val) {
	var vals = val.split("=");
	switch (vals[0].toLowerCase() )
	{
		case "example" :
			Advent.params.DataSet= "Example";
			break;
		case "part"	: 
			Advent.params.Part = vals[1];
			break;
		case "day" :
			Advent.params.Day = ("0" + vals[1]).substr(-2,2);
			break;
	}
});
var DayAdvent = require('./Day' + Advent.params.Day + "/DayAdvent");
var md5 = require('./md5');

Advent = Advent.extend(Advent,  DayAdvent);
var inputLoader = undefined;
var readmeLoader = Advent.LoadReadMe(); 



if ( Advent.params.DataSet === "Final" )
	inputLoader = Advent.LoadInput();
else 
	inputLoader = Advent.LoadDefaultInput();
readmeLoader.then(function(resp) {
	console.log(resp);
	inputLoader.then(function(resp) { 
		Advent.input = resp;
	}).then(function(resp) {
		console.log("running...");

		if ( Advent.params.Part == "1" )
			Advent.Part1();
		else
			Advent.Part2();
		

	})
	
}).catch( function(err) { console.log("Failed"); console.log(err);})
