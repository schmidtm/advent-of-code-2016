var DayAdvent = {
	startNum:[1,1],
	keyPad: [[1,2,3],[4,5,6],[7,8,9]],
	keyPadPart2: [
		[,,1,,,],
		[,2,3,4,],
		[5,6,7,8,9],
		[,'A','B','C',],
		[,,'D',,,]
	],
	calcNum: function()
	{
		return this.keyPad[this.startNum[0]][this.startNum[1]];
	},
	calcNumPart2: function(curNdx)
	{
		if ( curNdx === undefined)
			curNdx = this.startNum;
		try { 
			return this.keyPadPart2[curNdx[0]][curNdx[1]];
		} catch (e) {
			return undefined;
		}
	},
	makeMovePart2: function(direction)
	{
		var stepPos = 1;
		if ( direction === "U" || direction === "L")
			stepPos = -1;
		var stepDir = 0;
		if ( direction === "L" || direction === "R")
			stepDir = 1;
		var curNdx = [this.startNum[0], this.startNum[1]];
		var curPos = this.startNum[stepDir];
		var newPos = Math.min(Math.max(0, curPos + stepPos ), 5);
		curNdx[stepDir] = newPos;

		if ( this.calcNumPart2(curNdx) !== undefined)
			this.startNum[stepDir] = newPos;


		
	},
	makeMove: function(direction)
	{
		var stepPos = 1;
		if ( direction === "U" || direction === "L")
			stepPos = -1;
		var stepDir = 0;
		if ( direction === "L" || direction === "R")
			stepDir = 1;
		var curPos = this.startNum[stepDir];
		var newPos = Math.min(Math.max(0, curPos + stepPos ), 2);
		
		this.startNum[stepDir] = newPos;
	},
	resetPuzzle: function()
	{
		this.startNum = [1,1];
	},
	resetPuzzlePart2: function()
	{
		this.startNum = [2,0];
	},
	performPart1: function(output, currentValue, currentIndex)
	{
		if ( currentIndex === 0)
		{
			this.resetPuzzle();
		}

		/*
		var steps = [...currentValue];
		*/
		var steps = currentValue.split("");
		steps.forEach(this.makeMove.bind(this));
		var res = this.calcNum();
		output.push(JSON.stringify(res));
			
			
			
			
		return output;
	},
	performPart2: function(output, currentValue, currentIndex)
	{
		if ( currentIndex === 0)
		{
			this.resetPuzzlePart2();
		}
		/*
		ES6 Only not working IE 11, but works in edge
		var steps = [...currentValue];
		*/
		
		var steps = currentValue.split("");
		steps.forEach(this.makeMovePart2.bind(this));
		var res = this.calcNumPart2();
		output.push(JSON.stringify(res));
			
			
			
			
		return output;

	}
	
}



try {

 	module.exports = DayAdvent;

} catch (e) {

	Advent = Advent.extend(Advent,  DayAdvent);
}
