var DayAdvent = {
	expandLetter: function(strIn)
	{
		var matcher = /\((\d+)x(\d+)\)/;
		var begSearch = 0;
		var begStrs = [];
		var matches = undefined;
		while ( matches =matcher.exec(strIn) )
		{
			var locOfString = matches.index;
			var numChars = matches[0].length; 
			var repeatNumTimes = matches[2]*1;
			var lengthOfMatch = matches[1]*1;
			var strToRepeat = strIn.substr(locOfString + numChars, lengthOfMatch);
			var insStr = this.repeatString(strToRepeat, repeatNumTimes);
			var tmpStr = strIn.substr(0, locOfString);
			begStrs.push(tmpStr);
			begStrs.push(insStr);
			strIn = strIn.substr(locOfString + numChars + strToRepeat.length);
			
		}
		begStrs.push(strIn);
		return begStrs.join("");
	},
	repeatString: function(strIn, numTimes)
	{
		var arr = new Array(numTimes);
		/* would use fill if available in IE */
		for ( var i =0; i < numTimes;i++)
		{
			arr[i] = strIn;
		}
		return arr.join("");
	},
	performPart1: function(output, currentValue)
	{
		var outStr = this.expandLetter(currentValue);
		
		output.push(outStr);
		output.push("Length::" + outStr.replace(/\s+/gi, "").length);
			
		/* ADVENT contains no markers and decompresses to itself with no changes, resulting in a decompressed length of 6.
A(1x5)BC repeats only the B a total of 5 times, becoming ABBBBBC for a decompressed length of 7.
(3x3)XYZ becomes XYZXYZXYZ for a decompressed length of 9.
A(2x2)BCD(2x2)EFG doubles the BC and EF, becoming ABCBCDEFEFG for a decompressed length of 11.
(6x1)(1x3)A simply becomes (1x3)A - the (1x3) looks like a marker, but because it's within a data section of another marker, it is not treated any differently from the A that comes after it. It has a decompressed length of 6.
X(8x2)(3x3)ABCY becomes X(3x3)ABC(3x3)ABCY (for a decompressed length of 18), because the decompressed data from the (8x2) marker (the (3x3)ABC) is skipped and not processed further.
What is the decompressed length of the file (your puzzle input)? Don't count whitespace.
*/
			
			
		return output;
	},
	performPart2: function(output, currentValue)
	{
		var res = {"itm":currentValue};
		output.push(JSON.stringify(res));
			
			
		/*
Length x Repeat

Depth search find next result have a buffer of processing

keep count of calculated		
(3x3)XYZ still becomes XYZXYZXYZ, as the decompressed section contains no markers.

X(8x2)(3x3)ABCY becomes XABCABCABCABCABCABCY, because the decompressed data from the (8x2) marker is then further decompressed, 
thus triggering the (3x3) marker twice for a total of six ABC sequences.
(27x12)(20x12)(13x14)(7x10)(1x12)A decompresses into a string of A repeated 241920 times.
(25x3)(3x3)ABC(2x3)XY(5x2)PQRSTX(18x9)(3x2)TWO(5x7)SEVEN becomes 445 characters long.
      12345678911234567892123456789312345678941234567895
 
	  25 is through T
	  9+18+30 + 1 
	  18x9 i sthorugh rest
	  6*9+35*9
	  54+315

*/
	
			
		return output;

	}
	
}
try {

 	module.exports = DayAdvent;

} catch (e) {

	Advent = Advent.extend(Advent,  DayAdvent);
}
