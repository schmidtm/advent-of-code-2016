var DayAdvent = {
	compareNumbers: function(a,b)
	{
		return a - b;
	},
	isTriangle: function(side1, side2, side3)
	{
		var arr = [side1, side2, side3];
		arr.sort(this.compareNumbers.bind(this));
		if ( (arr[0] + arr[1]) > arr[2] )
		{
			return true;
		}
		return false;
	},
	performPart1: function(output, currentValue)
	{
		currentValue = this.trim(currentValue);
		var res = "Bad";

		var numbers = currentValue.split(/\s+/).map(Number);
		
	//	numbers.sort(this.compareNumbers.bind(this));
		if ( this.isTriangle(numbers[0], numbers[1], numbers[2]) )
		{
			res = "Valid";
		
			output.push(res);
		}	
			
			
			
		return output;
	},
	performPart2: function(output, currentValue)
	{
		var res = {"itm":currentValue};
		output.push(JSON.stringify(res));
			
			
			
			
		return output;

	},
	Part1: function()
	{
		
			
		var vals = this.GetInput().split("\n");
		var outStr = [];
		
		 outStr = vals.reduce(this.performPart1.bind(this), []);
		
		outStr.push("Valid: " + outStr.length);
		outStr.push("end");
		this.WriteOutput(outStr);
	
	},
	Part2: function()
	{
		var vals = this.trim(this.GetInput()).split(/\D+/gi).map(Number);
		/* array of digits assumption three columns */

		var outStr = [];
		var triangleCount = 0;
		for ( var i = 0; i < vals.length / 9; i++)
		{
			var currentIndex = i*9;
			for ( var j=0;j<3;j++)
			{
				if ( this.isTriangle(vals[currentIndex+(j+0)], vals[currentIndex+(j+3*1)], vals[currentIndex+(j+3*2)]) ){
						triangleCount++;
				}

			}
		}


		 /* outStr = vals.reduce(this.performPart2.bind(this), []); */
		
		
		outStr.push("Valid: " + triangleCount);
		outStr.push("end");
		this.WriteOutput(outStr);
	}
}



try {

 	module.exports = DayAdvent;

} catch (e) {

	Advent = Advent.extend(Advent,  DayAdvent);
}
