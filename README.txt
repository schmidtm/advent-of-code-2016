Solutions for: http://adventofcode.com

To Work with template duplicate DayTemplate Folder and then modify the files in them as seen fit

Example Calling from nodejs

node Advent.njs Day=8 Part=2 Example
    /* Run Day 8 Part 2 Example DataSet */

node Advent.njs Day=8 Part=1
    /* Run Day 8 Part 1 Full DataSet */